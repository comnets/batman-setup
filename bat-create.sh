#!/usr/bin/env bash

interface=wlan0
channel=1
essid=bat-hive
ap=

# depmod -a
modprobe batman-adv

usage() {
    echo  "$0 [-i|--interface interface] [-c|--channel channel_no] [--essid essid]"
}

while [ "$1" != "" ]; do
    case $1 in
        -i | --interface )      shift
                                interface=$1
                                ;;
        -c| --channel )         shift
                                channel=$1
                                ;;
        --essid )               shift
                                essif=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


ip link set down dev ${interface}
ip link set mtu 1532 dev ${interface}

if [ "$ap" = "" ]; then
    iwconfig ${interface} mode ad-hoc essid ${essid} channel ${channel}
else
    iwconfig ${interface} mode ad-hoc essid ${essid} ap ${ap} channel ${channel}
fi

batctl if add ${interface}
ip link set up dev ${interface}
ip link set up dev bat0
