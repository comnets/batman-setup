#!/usr/bin/env bash

wget -O /tmp/batman-adv-2017.1.tar.gz https://downloads.open-mesh.org/batman/stable/sources/batman-adv/batman-adv-2017.1.tar.gz
wget -O /tmp/batctl-2017.1.tar.gz     https://downloads.open-mesh.org/batman/stable/sources/batctl/batctl-2017.1.tar.gz
cd /tmp
tar -xvzf batman-adv-2017.1.tar.gz
tar -xvzf batctl-2017.1.tar.gz

cd /tmp/batman-adv-2017.1
make make install

cd /tmp/batctl-2017.1/
make
make install

