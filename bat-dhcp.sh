#!/usr/bin/env bash

net=10.8.0.1/24

while [ "$1" != "" ]; do
    case $1 in
        -n | --network )        shift
                                net=$1
                                echo Make sure to change /etc/dhcpd.conf to reflect network address
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


ip link set up dev bat0
ip addr add ${net} dev bat0

mv dhcpd.conf.example /etc/dhcpd.conf

systemctl start dhcpd4.service
